<?php

session_start();

session_cache_limiter();
date_default_timezone_set('America/Argentina/Buenos_Aires');
header("Content-Type: text/html; charset=iso-8859-1");

require_once ('conexion.php');


$request = json_decode(file_get_contents('php://input')); 


// Funcion Traer el Proximo Id para IdDetalle
function getMaxIdDetalle($conn, $IdCotizacion) {
  $query = "SELECT MAX(IdDetalle)+1 maxIdDetalle FROM CotizacionDetalle WHERE IdCotizacion = $IdCotizacion ";
  $rs_maxIdDetalle = sqlsrv_query($conn, $query);
  $row_maxIdDetalle = sqlsrv_fetch_array($rs_maxIdDetalle);
  $maxIdDetalle = $row_maxIdDetalle['maxIdDetalle'];
  if ($maxIdDetalle == '') {
    $maxIdDetalle = 1;
  }
  //echo "$IdCotizacion - $maxIdDetalle";
  return $maxIdDetalle;
}

// Funcion Traer el Proximo Id para IdCotizacion
function getMaxIdCotizacion($conn) {
  $query = "SELECT MAX(IdCotizacion)+1 maxIdCotizacion FROM Cotizaciones";
  $rs_maxIdCotizacion = sqlsrv_query($conn, $query);
  $row_maxIdCotizacion = sqlsrv_fetch_array($rs_maxIdCotizacion);
  $maxIdCotizacion = $row_maxIdCotizacion['maxIdCotizacion'];
  if ($maxIdCotizacion == '') {
    $maxIdCotizacion = 1;
  }
  return $maxIdCotizacion;
}


if ($_SESSION['IdUsuario'] <> '') {

  // CARGAMOS EL ENCABEZADO DE LA COTIZACION

  $query = "USE Paquetes";
  $exec_query = sqlsrv_query($conn, $query);
  //echo "<br>query: ".$query;

  $query = "SELECT Numero+1 ProxNumero FROM Numero";
  $rs_Numero = sqlsrv_query($conn, $query);
  $row_Numero = sqlsrv_fetch_array($rs_Numero);
  $Numero = $row_Numero['ProxNumero'];

  
  $IdUsuario = $_SESSION['IdUsuario'];
  $RazonSocial = $request->razon_social;
  $Celular = $request->celular;
  $Email = $request->email;
  $Localidad = $request->localidad;
  $Domicilio = $request->domicilio;
  $maxIdCotizacion = getMaxIdCotizacion($conn);
  $modo_adquisicion = $request->modo_adquisicion;

  if ($modo_adquisicion == 'v') {
    $modo_adquisicion_txt = 'VENTA';
  }
  else {
    $modo_adquisicion_txt = 'ALQUILER';
  }

  $hash = rand(10000, 999999);
  $nombre_del_pdf = 'cotizacion-clase-x-nro-'.$Numero.'-'.$hash.'.pdf';


  $query = "INSERT INTO Cotizaciones (IdCotizacion, Fecha, Numero, IdUsuario, RazonSocial, Domicilio, Telefono, eMail, Localidad, nombre_del_pdf, modo_adquisicion) VALUES ($maxIdCotizacion, GETDATE(), $Numero, $IdUsuario, '$RazonSocial', '$Domicilio', '$Celular', '$Email', '$Localidad', '$nombre_del_pdf', '$modo_adquisicion_txt')";
  $exec_query = sqlsrv_query($conn, $query);
  //echo "<br>query: ".$query;

  $query = "UPDATE Numero SET Numero = $Numero";
  sqlsrv_query($conn, $query);
  //echo "<br>query: ".$query;


  $query = "SELECT TOP 1 IdCotizacion FROM Cotizaciones WHERE IdUsuario = $IdUsuario ORDER BY IdCotizacion DESC";
  $rs_Cotizacion = sqlsrv_query($conn, $query);
  $row_Cotizacion = sqlsrv_fetch_array($rs_Cotizacion);
  $IdCotizacion = $row_Cotizacion['IdCotizacion'];


  // CARGAMOS LAS LINEAS DE LA COTIZACION

  $subtotal = 0;

  for($i=0; $i<count($request->paquetes_erp); $i++) { 
    $IdArticulo = $request->paquetes_erp[$i]->IdArticulo;
    $Nombre = $request->paquetes_erp[$i]->Nombre;
    $chequeado = $request->paquetes_erp[$i]->chequeado;
    $ModoAdquisicion_paquete = $request->paquetes_erp[$i]->ModoAdquisicion;
    //echo "<br> $Nombre: ($chequeado)";
    if ($chequeado AND $ModoAdquisicion_paquete == $modo_adquisicion) {
      
      $query = "SELECT Nombre FROM Paquetes2017 WHERE IdArticulo = $IdArticulo";
      $rs_Articulo = sqlsrv_query($conn, $query);
      $row_Articulo = sqlsrv_fetch_array($rs_Articulo);
      $Descripcion = $row_Articulo['Nombre'];

      $Precio = $request->paquetes_erp[$i]->Precio;
      $PrecioBase = $Precio*0.79;
      $maxIdDetalle = getMaxIdDetalle($conn, $maxIdCotizacion);
      //$subtotal = $subtotal + $Precio;

      $query = "INSERT INTO CotizacionDetalle (IdCotizacion, IdDetalle, IdArticulo, Descripcion, PrecioBase, PrecioFinal, Cantidad) VALUES ($maxIdCotizacion, $maxIdDetalle, $IdArticulo, '$Descripcion', $PrecioBase, $Precio, 1)";
      $exec_query = sqlsrv_query($conn, $query);
      //echo "<br>query: ".$query;
    }
  }

  $subtotal = $subtotal * $request->servidor_erp_cant;



  // SERVIDORES 
  $servidor_erp_cant = $request->servidor_erp_cant;
  $IdArticulo = 'NULL';
  $servidor_erp = $request->servidor_erp; 
  $Descripcion = 'Servidor';
  $PrecioFinal = $servidor_erp;
  $PrecioBase = $servidor_erp*0.79;
  $maxIdDetalle = getMaxIdDetalle($conn, $maxIdCotizacion);
  $subtotal = $subtotal + ($PrecioFinal*$servidor_erp_cant);


  $query = "INSERT INTO CotizacionDetalle (IdCotizacion, IdDetalle, IdArticulo, Descripcion, PrecioBase, PrecioFinal, Cantidad) VALUES ($maxIdCotizacion, $maxIdDetalle, $IdArticulo, '$Descripcion', $PrecioBase, $PrecioFinal, $servidor_erp_cant)";
  $exec_query = sqlsrv_query($conn, $query);


  // TERMINALES 
  $terminales_erp_cant = $request->terminales_erp_cant;

  $IdArticulo = 'NULL';
  $terminales_erp_precio = $request->terminales_erp_precio; 

  $Descripcion = 'Terminales Adicionales';
  $PrecioFinal = $terminales_erp_precio;
  $PrecioBase = $terminales_erp_precio*0.79;
  $maxIdDetalle = getMaxIdDetalle($conn, $maxIdCotizacion);
  $subtotal = $subtotal + ($PrecioFinal*$terminales_erp_cant);


  $query = "INSERT INTO CotizacionDetalle (IdCotizacion, IdDetalle, IdArticulo, Descripcion, PrecioBase, PrecioFinal, Cantidad) VALUES ($maxIdCotizacion, $maxIdDetalle, $IdArticulo, '$Descripcion', $PrecioBase, $PrecioFinal, $terminales_erp_cant)";
  $exec_query = sqlsrv_query($conn, $query);

  // SUCURSALES
  $sucursales_erp_cant = $request->sucursales_erp_cant;

  if($sucursales_erp_cant>0) {

    $IdArticulo = $request->sucursales_erp_id;
    $sucursales_erp_precio = $request->sucursales_erp_precio; 

    $Descripcion = 'Sucursales';
    $PrecioFinal = $sucursales_erp_precio;
    $PrecioBase = $sucursales_erp_precio*0.79;
    $maxIdDetalle = getMaxIdDetalle($conn, $maxIdCotizacion);
    $subtotal = $subtotal + ($PrecioFinal*$sucursales_erp_cant);


    $query = "INSERT INTO CotizacionDetalle (IdCotizacion, IdDetalle, IdArticulo, Descripcion, PrecioBase, PrecioFinal, Cantidad) VALUES ($maxIdCotizacion, $maxIdDetalle, $IdArticulo, '$Descripcion', $PrecioBase, $PrecioFinal, $sucursales_erp_cant)";
    //echo $query ;
    $exec_query = sqlsrv_query($conn, $query);


  }
  

  // HONORARIOS PROFESIONALES
  $honorarios_de_implementacion = $request->honorarios_de_implementacion;

 

  $IdArticulo = 'NULL';
  $Descripcion = 'Honorarios de implementacion';
  $PrecioFinal = $honorarios_de_implementacion;
  $PrecioBase = $honorarios_de_implementacion*0.79;
  $maxIdDetalle = getMaxIdDetalle($conn, $maxIdCotizacion);
  $subtotal = $subtotal + $PrecioFinal;

    $query = "INSERT INTO CotizacionDetalle (IdCotizacion, IdDetalle, IdArticulo, Descripcion, PrecioBase, PrecioFinal, Cantidad) VALUES ($maxIdCotizacion, $maxIdDetalle, $IdArticulo, '$Descripcion', $PrecioBase, $PrecioFinal, 1)";
    $exec_query = sqlsrv_query($conn, $query);


  // DESCUENTOS
  $descuento_porcentaje = $request->descuento_porcentaje;
  $descuento_importe = $request->descuento_importe;

  //echo '$subtotal: '.$subtotal;

  $PrecioFinalDescuento = 0;
  if ($descuento_porcentaje > 0) {
    $PrecioFinalDescuento = ($descuento_porcentaje*$subtotal/100)*-1;
  }
  if ($descuento_importe > 0) {
    $PrecioFinalDescuento = $PrecioFinalDescuento - $descuento_importe;
  }

  if ($PrecioFinalDescuento < 0) {

    $IdArticulo = 'NULL';
    $Descripcion = 'Importe de Descuento';
    $PrecioBase = $PrecioFinalDescuento*0.79;
    $maxIdDetalle = getMaxIdDetalle($conn, $maxIdCotizacion);

      $query = "INSERT INTO CotizacionDetalle (IdCotizacion, IdDetalle, IdArticulo, Descripcion, PrecioBase, PrecioFinal, Cantidad) VALUES ($maxIdCotizacion, $maxIdDetalle, $IdArticulo, '$Descripcion', $PrecioBase, $PrecioFinalDescuento, 1)";
      $exec_query = sqlsrv_query($conn, $query);
  }



  echo $IdCotizacion;
}
else {
  echo json_encode('error');  
}
?>