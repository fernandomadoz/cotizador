<?php 

session_start();

require_once ('conexion.php');

if (isset($_POST['usuario']) and $_POST['clave']) {
  $usuario = $_POST['usuario'];
  $clave = $_POST['clave'];

  $sql = "SELECT * FROM Usuarios WHERE Usuario = '$usuario' AND Clave = '$clave'";
  $params = array();
  $options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
  $rs_usuarios = sqlsrv_query( $conn, $sql , $params, $options );

  $row_count = sqlsrv_num_rows( $rs_usuarios );

  if ($row_count == 1) {
    $row_usuarios = sqlsrv_fetch_array($rs_usuarios);
    $_SESSION['IdUsuario'] = $row_usuarios['IdUsuario'];
    $_SESSION['Vendedor'] = $row_usuarios['Vendedor'];
  }
  else {
    header("Location: login.php?e=x");  
  }
}


session_cache_limiter();
date_default_timezone_set('America/Argentina/Buenos_Aires');
header("Content-Type: text/html; charset=iso-8859-1");


?>
<!doctype html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="content-language" content="es">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/logo_shortcut.ico" />

    <title>Presupuestador Clase-X</title>


    <!-- jquery -->
    <script src="libs/jquery/jquery.min.js"></script>

    <!-- Bootstrap -->
    <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="libs/bootstrap/js/bootstrap.js"></script>


    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/typeahead.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee|Montserrat" rel="stylesheet">
    <script src="libs/vue/vue.js"></script>
    <script src="libs/typeahead.bundle.js"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
    <script src="libs/vee-validate/dist/vee-validate.js"></script>
    <script src="libs/vee-validate/dist/locale/es.js"></script>
    <!--script src="libs/vee-validate/flow/validator.js"></script-->

  </head>

  <body>

    <div id="encabezado">
      <div id="centro">
        <!-- Encabezado Logos -->
        <div class="container contenedor_encabezado">
          <div class="row">
            <img class="hidden-xs" src="img/logo.png">
            <img class="visible-xs pull-left img-top-xs" src="img/logo.png">
            <img class="hidden-xs  pull-right" src="img/titulo-presupuestador.png">
            <img class="visible-xs  pull-right img-top-xs" src="img/titulo-presupuestador.png">
          </div>
        </div>
        <!-- Fin Encabezado Logos -->

        <!-- Inicio Contenedor Principal -->
        <div class="container contenedor_principal">


        <?php if ($_SESSION['IdUsuario'] <> '') { ?>

          <!-- INICIO APP vue.js -->
          <div id="app">
            <form action="procesar-cotizacion.php" method="POST">

              <!-- Modo de Adquisición -->
               <div class="col-xs-12 col-lg-6">

                <!-- Modo de Adquisición LG -->
                <div class="btn-group modo-de-adquisicion">
                  <label class="btn btn-lg btn-primary btn-modo">
                    <input type="radio" name="modo_adquisicion" value="v" autocomplete="off" v-model="modo_adquisicion" id="btn-Modo-Venta" checked="checked" v-on:change="limpiar_paquetes"> Venta
                  </label>
                  <label class="btn btn-lg btn-primary btn-modo">
                    <input type="radio" name="modo_adquisicion" value="a" autocomplete="off" v-model="modo_adquisicion" id="btn-Modo-Alquiler" v-on:change="limpiar_paquetes"> Alquiler
                  </label>
                </div>
                

              </div>
              <!-- FIN Modo de Adquisición -->



              <!-- Datos del Cliente -->
              <div class="col-xs-12 col-lg-12">    

                <div class="panel panel-default">
                  <div class="panel-heading panel-heading-cliente" data-toggle="collapse" data-parent="#accordion" href="#collapseCliente" aria-expanded="true" aria-controls="collapseCliente">
                    <h3 class="tit-panel"><strong>Dirigido a:</strong> {{ razon_social }}</h3>
                  </div>
                  <div id="collapseCliente" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingCliente">
                    <div class="panel-body">

                      <div class="col-xs-12 col-lg-6"> 
                        <div class="form-group">
                          <label for="razon_social">Razon Social *</label>
                          <input v-validate="'required'" type="text" class="form-control" id="razon_social" name="Razon Social" v-model="razon_social" placeholder="Razon Social" required="required">
                          <span v-show="errors.has('Razon Social')" class="text-danger">{{ errors.first('Razon Social') }}</span>
                        </div>
                        <div class="form-group">
                          <label for="celular">Tel&eacute;fono Celular * (ej: 3462602230)</label>
                          <input v-validate="'regex:^([0-9]+)$|required'" type="number" class="form-control" id="celular" name="Celular" v-model="celular" placeholder="Tel&eacute;fono Celular" required="required" min="0">
                          <span v-show="errors.has('Celular')" class="text-danger">{{ errors.first('Celular') }}</span>
                        </div>
                        <div class="form-group">
                          <label for="Email">Email *</label>
                            <input v-validate="'required|email'"  name="Email" type="text" class="form-control" placeholder="Email" v-model="email">
                            <span v-show="errors.has('Email')" class="text-danger">{{ errors.first('Email') }}</span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-lg-6">                         
                        <div class="form-group">
                          <label for="localidad">Domicilio *</label>                          
                          <input v-validate="'required'" type="text" class="form-control" id="domicilio" name="Domicilio" v-model="domicilio" placeholder="Domicilio" required="required">   
                          <span v-show="errors.has('Domicilio')" class="text-danger">{{ errors.first('Domicilio') }}</span>                       
                        </div>                        
                        <div class="form-group">
                          <label for="localidad">Localidad *</label>                          
                          <input v-validate="'required'" type="text" class="form-control" id="localidad" name="Localidad" v-model="localidad" placeholder="Localidad" required="required">  
                          <span v-show="errors.has('Localidad')" class="text-danger">{{ errors.first('Localidad') }}</span>                        
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

              </div>
              <!-- FIN Datos del Cliente -->


              <!-- Modulo geXtion ERP -->
              <div class="col-xs-12 col-lg-12">    

                <div class="panel panel-default">
                  <div class="panel-heading panel-heading-erp" >

                    <!-- Encabezado LG -->
                    <div class="hidden-xs">
                      <div data-toggle="collapse" data-parent="#accordion" href="#collapseERP" aria-expanded="true" aria-controls="collapseERP">
                        <img src="img/logo_gextion_erp.fw.png">
                      </div>
                      <div class=" btn-link-paquete pull-right">
                        <a href="http://www.clase-x.com.ar/gextion.php" target="_blank">
                          <button type="button" class="pull-right btn btn-default btn-md">
                            <span class="glyphicon glyphicon-link" aria-hidden="true"></span> Link
                          </button>
                        </a>
                        <a href="http://www.clase-x.com.ar/productos/pdf/gextion.pdf" target="_blank">
                          <button type="button" class="pull-right btn btn-default btn-md">
                            <span class="glyphicon glyphicon-download" aria-hidden="true"></span> PDF
                          </button>
                        </a>
                      </div>                      
                    </div>

                    <!-- Encabezado XS -->
                    <div class="visible-xs">
                      <div data-toggle="collapse" data-parent="#accordion" href="#collapseERP" aria-expanded="true" aria-controls="collapseERP">
                        <img class="img-gextion-xs" src="img/logo_gextion_erp.fw.png">
                      </div>
                      <div class=" btn-link-paquete-xs pull-right">
                        <a href="http://www.clase-x.com.ar/gextion.php" target="blank">
                          <button type="button" class="pull-right btn btn-default btn-xs">
                            <span class="glyphicon glyphicon-link" aria-hidden="true"></span> 
                          </button>
                        </a>
                        <a href="http://www.clase-x.com.ar/productos/pdf/gextion.pdf" target="blank">
                          <button type="button" class="pull-right btn btn-default btn-xs">
                            <span class="glyphicon glyphicon-download" aria-hidden="true"></span> 
                          </button>
                        </a>
                      </div>                      
                    </div>


                  </div>
                  <div id="collapseERP" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingERP">
                    <div class="panel-body">


                      <!-- Encabezados -->
                      <div class="col-lg-6">
                        <div class="col-paquete div-col-enc" style="margin-top: 10px; margin-bottom: 10px;">Paquete</div>
                        <div class="col-cant div-col-enc">Cant</div>
                        <div class="col-precio div-col-enc">Precio</div>
                      </div>

                      <div class="col-lg-6 hidden-xs hidden-md hidden-sm">
                        <div class="col-paquete div-col-enc" style="margin-top: 10px; margin-bottom: 10px;">Paquete</div>
                        <div class="col-cant div-col-enc">Cant</div>
                        <div class="col-precio div-col-enc">Precio</div>
                      </div>
                      <!-- Fin Encabezados -->




                      <!-- Paquetes -->
                      <div v-for="(paquete, key, index) in paquetes_erp">
                        <div class="col-lg-6 fila-paquete" v-if="paquete.ModoAdquisicion == modo_adquisicion">
                          <div class="col-paquete">
                            <div class="checkbox">
                              <label 
                              v-bind:tabindex="paquete.key" 
                              role="button" 
                              data-toggle="popover" 
                              data-html="true" 
                              data-trigger="focus" 
                              title="Caracteristicas" 
                              data-template='<div class="popover" style="width: 400px; max-width: 600px;" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
                              v-bind:data-content="paquete.caracteristicas_html" >
                                <input type="checkbox" v-model="paquete.chequeado" v-bind:name="'paq_erp_'+paquete.IdArticulo"  v-bind:id="'paq_erp_'+paquete.IdArticulo"  onclick="validar_check(this.name)"> {{ paquete.Nombre }}
                              </label>
                            </div>
                          </div>
                          <div class="col-cant">1</div>
                          <div class="col-precio">$ {{ paquete.Precio | formatoMoneda }}</div>
                        </div>
                      </div>



                      <div class="col-lg-12">
                        <hr>
                      </div>

                      <div class="col-lg-6">
                        
                        <!-- Servidor -->
                        <div class="col-paquete">
                          <div class="checkbox">
                              Servidor
                          </div>
                        </div>
                        <div class="col-cant">
                          <input v-validate="'regex:^([0-9]+)$|required'" class="form-control text-right" type="number" min="1" max="999" v-model="servidor_erp_cant" name="servidor_erp_cant">
                          <input type="hidden" v-model="servidor_erp" name="servidor_erp">
                          <span v-show="errors.has('servidor_erp_cant')" class="text-danger">{{ errors.first('servidor_erp_cant') }}</span>
                        </div>
                        <div class="col-precio">$ {{ servidor_erp | formatoMoneda }}</div>

                      


                        <!-- Terminales Adicionales -->
                        <div class="col-paquete">
                          <div class="checkbox">
                              Terminales Adicionales
                          </div>
                        </div>
                        <div class="col-cant">
                          <input v-validate="'regex:^([0-9]+)$|required'" class="form-control text-right" type="number" min="0" max="999" v-model="terminales_erp_cant" name="terminales_erp_cant">
                          <span v-show="errors.has('terminales_erp_cant')" class="text-danger">{{ errors.first('terminales_erp_cant') }}</span>
                        </div>
                        <div class="col-precio">$ {{ terminales_erp_precio | formatoMoneda }}</div>
                        <input type="hidden" v-model="terminales_erp_cant" name="terminales_erp_cant">
                        <input type="hidden" v-model="terminales_erp_precio" name="terminales_erp_precio">
                      


                        <!-- Sucursales -->

                        <div class="col-paquete">
                          <div class="checkbox">
                            Sucursales
                          </div>
                        </div>
                        <div class="col-cant">
                          <input v-validate="'regex:^([0-9]+)$|required'" class="form-control text-right" type="number" min="0" max="999" v-model="sucursales_erp_cant" name="sucursales_erp_cant">
                          <span v-show="errors.has('sucursales_erp_cant')" class="text-danger">{{ errors.first('sucursales_erp_cant') }}</span>
                        </div>
                        <div class="col-precio" v-if="modo_adquisicion == 'v'">
                          $ {{ sucursales_erp_precio_venta | formatoMoneda }}
                          <input type="hidden" v-model="sucursales_erp_id_venta" name="sucursales_erp_id">
                          <input type="hidden" v-model="sucursales_erp_precio_venta" name="sucursales_erp_precio">
                        </div>
                        <div class="col-precio" v-if="modo_adquisicion == 'a'">
                          $ {{ sucursales_erp_precio_alquiler | formatoMoneda }}
                          <input type="hidden" v-model="sucursales_erp_id_alquiler" name="sucursales_erp_id">
                          <input type="hidden" v-model="sucursales_erp_precio_alquiler" name="sucursales_erp_precio">
                        </div>


                        <!-- Honorarios de implementacion -->
                        <div class="col-paquete">
                          <div class="checkbox">
                              Honorarios de implementaci&oacute;n <span class="glyphicon glyphicon-info-sign
" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Servidor + Terminales Adicionales + Sucursales"></span>
                          </div>
                        </div>
                        <div class="col-cant-precio">
                          <input v-validate="'regex:^([0-9]+)$|required'" class="form-control text-right" type="number" min="0" max="99999999" v-model="honorarios_de_implementacion" name="honorarios_de_implementacion">
                          <span v-show="errors.has('honorarios_de_implementacion')" class="text-danger">{{ errors.first('honorarios_de_implementacion') }}</span>
                        </div>
                      

                        <!-- Descuento -->
                        <div class="col-paquete">
                          <div class="checkbox">
                              Porcentaje de Descuento
                          </div>
                        </div>
                        <div class="col-cant-precio">
                          <input v-validate="'regex:^([0-9]+)$|required'" class="form-control text-right" type="number" min="0" max="100" v-model="descuento_porcentaje" name="descuento_porcentaje">
                          <span v-show="errors.has('descuento_porcentaje')" class="text-danger">{{ errors.first('descuento_porcentaje') }}</span>
                        </div>


                        <!-- Descuento -->
                        <div class="col-paquete">
                          <div class="checkbox">
                              Importe de Descuento
                          </div>
                        </div>
                        <div class="col-cant-precio">
                          <input v-validate="'regex:^([0-9]+)$|required'" class="form-control text-right" type="number" min="0" v-model="descuento_importe" name="descuento_importe">
                          <span v-show="errors.has('descuento_importe')" class="text-danger">{{ errors.first('descuento_importe') }}</span>
                        </div>
                      </div>


                   
                    </div>
                  </div>
                  <div class="panel-footer panel-footer-erp">
                    <h3 class="text-right">Total: $ {{ subtotal_erp | formatoMoneda }} </h3>
                    <input type="hidden" v-model="subtotal_erp" name="subtotal_erp">
                  </div>
                </div>

              </div>
              <!-- FIN Modulo geXtion ERP -->

              <div class="col-xs-12 col-lg-12">
                
                <button type="button" class="btn btn-lg btn-default btn-accion" aria-label="Left Align">
                  <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true" v-on:click="validar_errores"> Generar Cotizaci&oacute;n</span>
                </button>
                
                <a v-bind:href="url_pdf" target="_blank">
                  <transition name="fade">
                    <button type="button" class="btn btn-lg btn-primary btn-accion" v-if="btn_descargar_pdf" aria-label="Left Align">
                      <span class="glyphicon glyphicon-save" aria-hidden="true"> Descargar PDF</span>
                    </button>
                  </transition>
                </a>

                <a v-bind:href="mensaje_whastapp" target="_blank">
                  <transition name="fade">
                    <div type="button" class="btn btn-lg btn-success btn-accion" v-if="btn_descargar_pdf" aria-label="Left Align">
                      <img src="img/logo-whatsapp.png" style="width: 21px"> Enviar via Whatsapp</span>
                    </div>
                  </transition>
                </a>
                
                <transition name="fade">
                  <button type="button" class="btn btn-lg btn-warning btn-accion" v-if="btn_descargar_pdf" v-on:click="EnviarCotizacionViaMail" aria-label="Left Align">
                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"> Enviar via Mail</span>
                  </button>
                </transition>

                <br> <br>

                <transition name="fade">
                  <div v-show="mostrar_mensaje" class="alert alert-success" role="alert">                    
                    {{ mensaje }}
                  </div>         
                </transition>
              
                <transition name="fade">
                  <div v-show="mostrar_mensaje_error" class="alert alert-danger" role="alert">
                    {{ mensaje_error }}
                  </div>              
                </transition>

                <!--transition name="fade">
                  <div class="alert alert-danger" role="alert">
                    {{ mensaje_envio_mail }}
                  </div>              
                </transition-->

              <a href="login.php?x">
                  <div class="btn btn-lg pull-right btn-danger">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"> Salir</span>
                  </div>
              </a>

                
              </div>
              <!--div class="col-lg-12">            
                <pre>{{ $data }}</pre>
              </div-->

            </form>

        





          </div>
          <!-- FIN APP vue.js -->
          <?php require_once('cotizador-vue.php'); ?>

          <!-- Popover Info Caracteristicas -->
          <script type="text/javascript">

          $( "#btn-Modo-Venta" ).blur(function () {
                 activarPopover()
                })
          $( "#btn-Modo-Alquiler" ).blur(function () {
                  activarPopover()
                })

            function activarPopover() {
              $('[data-toggle="popover"]').popover()
            }
            
            activarPopover();

            $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            })

            function validar_check(nombre_paquete) {
              click = true;
              //if (nombre_paquete.indexOf("1489") != -1 || nombre_paquete.indexOf("1846") != -1) {
              if (nombre_paquete.indexOf("1489") != -1 || nombre_paquete.indexOf("1774") != -1) {
                click = false;
                $( "#"+nombre_paquete ).prop('checked', true);
              }
              return click
            }

          </script>

          <?php }
          else {
            ?>

          <div class="alert alert-success cartel-mensaje" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Sesi&oacute;n Caducada, vuelva a loguearse <br><br>
            <a href="login.php"> <button class="btn btn-lg btn-primary"> ingresar </button> </a>
          </div>

          <?php } ?>
        </div>
        <!-- Fin Contenedor Principal -->

      </div>  <!-- /centro -->  
    </div>  <!-- /encabezado -->    





    
  </body>
</html>

