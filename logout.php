?php

session_start();

    session_unset();
    session_destroy();
    session_start();

?>

<!doctype html>
<html lang="es">
  <head>
    <meta http-equiv="content-language" content="es">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/logo_shortcut.ico" />

    <title>Login - Presupuestador Clase-X</title>

    <!-- Bootstrap core CSS -->
    <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee|Montserrat" rel="stylesheet">
  </head>

  <body>

    <div id="encabezado">
      <div id="centro">
        <div class="container contenedor_encabezado">
          <div class="row">
            <img src="img/logo.png">
            <img class="hidden-xs  pull-right" src="img/titulo-presupuestador.png">
            <img class="visible-xs  pull-right" style="width: 150px;" src="img/titulo-presupuestador.png">
          </div>
        </div>
        <div class="container contenedor_principal">



          <div class="alert alert-success cartel-mensaje" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Ha salido del Cotizador
          </div>



        </div> <!-- /container -->
      </div>  <!-- /centro -->  
    </div>  <!-- /encabezado -->    
  </body>
</html>

