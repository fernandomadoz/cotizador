<?php

include('conexion.php');

//$query = "SELECT *, CASE WHEN descripcion like 'venta%' THEN 'v' ELSE 'a' END ModoAdquisicion FROM Paquetes2017 WHERE  IdArticulo in (1509, 1865) ORDER BY IdArticulo";

$query = "SELECT *, CASE WHEN descripcion like 'venta%' THEN 'v' ELSE 'a' END ModoAdquisicion FROM Paquetes2017 WHERE  IdArticulo in (1509, 1794) ORDER BY IdArticulo";
$paquete_erp_suc = sqlsrv_query($conn, $query);
$row_paquetes_erp_suc = sqlsrv_fetch_array($paquete_erp_suc);
$sucursales_erp_id_venta = $row_paquetes_erp_suc['IdArticulo'];
$sucursales_erp_precio_venta = $row_paquetes_erp_suc['Precio'];
$row_paquetes_erp_suc = sqlsrv_fetch_array($paquete_erp_suc);
$sucursales_erp_id_alquiler = $row_paquetes_erp_suc['IdArticulo'];
$sucursales_erp_precio_alquiler = $row_paquetes_erp_suc['Precio'];

// $query = "SELECT *, CASE WHEN descripcion like 'venta%' THEN 'v' ELSE 'a' END ModoAdquisicion FROM Paquetes2017 WHERE  IdArticulo not in (1509, 1865)";
$query = "SELECT *, CASE WHEN descripcion like 'venta%' THEN 'v' ELSE 'a' END ModoAdquisicion FROM Paquetes2017 WHERE  IdArticulo not in (1509, 1749) AND IdArticuloRel IS NOT NULL";
$paquetes_erp = sqlsrv_query($conn, $query);

function arrToUl($arr) {
  $lista = '<ul>';
  for ($i=0; $i<count($arr); $i++) {
    $lista .= '<li>'.$arr[$i].'</li>';
  }
  $lista .= '</ul>';
  return $lista;
}

?>



<!-- vue.js -->
<script type="text/javascript">


const config = {
  locale: 'es', 
};


Vue.use(VeeValidate, config);
/*
const validator = new Validator({
  email: 'required|email',
  name: 'required|alpha|min:3'
});
*/

  //Vue.use(VeeValidate);

  Vue.mixin({
    created: function () {
      //var subtotal_mrp = this.$options.subtotal_mrp
    }
  })


  var app = new Vue({
    el: '#app',
    data: {
      razon_social: '',
      email: '',
      celular: '',
      localidad: '',
      domicilio: '',
      cotizacion_id: -1,
      url_pdf: '',
      mostrar_mensaje: false,
      mensaje: '',
      mensaje_whastapp: '',
      btn_descargar_pdf: false,
      mostrar_mensaje_error: false,
      mensaje_error: '',
      guardar: false,
      mensaje_envio_mail: '',
      paquetes_erp: [

        <?php 
        $i=0;
        while($row_paquetes_erp = sqlsrv_fetch_array($paquetes_erp)) { 
          $k++;

          $caracteristicas_html = ''; 

          $partes_mod = explode(";", $row_paquetes_erp['Modulos']);
          $partes_caract = explode(";", $row_paquetes_erp['Caracteristicas']);
          $countm = count($partes_mod);
          for ($j = 0; $j < $countm; $j++) {
            $partes_caract_lista = explode(',', $partes_caract[$j]);
            $caracteristicas_html .= "<p><b>".$partes_mod[$j]."</b>: ". arrToUl($partes_caract_lista)."</p>";
          }

          if($row_paquetes_erp['IdArticulo'] == 1489) {
            $chequeado = 'true';
          }
          else {
            $chequeado = 'false';
          }
          

        ?>
        {
          chequeado: <?php echo $chequeado; ?>,
          IdArticulo: <?php echo $row_paquetes_erp['IdArticulo']; ?>,
          Precio: <?php echo $row_paquetes_erp['Precio']; ?>,
          ModoAdquisicion: '<?php echo $row_paquetes_erp['ModoAdquisicion']; ?>',
          Nombre: '<?php echo $row_paquetes_erp['Nombre']; ?>',
          caracteristicas_html: '<?php echo $caracteristicas_html; ?>',
          IdArticuloRel: <?php echo $row_paquetes_erp['IdArticuloRel']; ?>
        }, 
        <?php } ?> 

      ],
      servidor_erp : 0,
      servidor_erp_cant: 1,
      terminales_erp_cant: 0,
      terminales_erp_precio: 0,
      sucursales_erp_cant : 0,
      sucursales_erp_id_venta : <?php echo $sucursales_erp_id_venta; ?>,
      sucursales_erp_precio_venta : <?php echo $sucursales_erp_precio_venta; ?>,
      sucursales_erp_id_alquiler : <?php echo $sucursales_erp_id_alquiler; ?>,
      sucursales_erp_precio_alquiler : <?php echo $sucursales_erp_precio_alquiler; ?>,
      honorarios_de_implementacion: 0,
      descuento_porcentaje: 0,
      descuento_importe: 0,
      total_general: 0


    },

    computed: {
        subtotal_erp: function () {
          subtotal = 0;

          for (var i = 0, len = this.paquetes_erp.length; i < len; i++) {
            item = this.paquetes_erp[i]
              if (item.chequeado && this.modo_adquisicion == item.ModoAdquisicion) {
                subtotal = subtotal + Number(item.Precio)
              }
            }

          this.servidor_erp = parseInt(subtotal)

          subtotal = subtotal * this.servidor_erp_cant

          //Mostrar el Precio por todas las terminales
          //this.terminales_erp_precio = this.servidor_erp * 0.25 * this.terminales_erp_cant;
          //subtotal = subtotal + this.terminales_erp_precio

          //Mostrar el Precio por cada terminal
          this.terminales_erp_precio = this.servidor_erp * 0.25
          subtotal = subtotal + (this.terminales_erp_cant * this.terminales_erp_precio)
          
          
          if (this.modo_adquisicion == 'v') {
            subtotal = subtotal + (this.sucursales_erp_precio_venta * this.sucursales_erp_cant)            
          }
          else {
            subtotal = subtotal + (this.sucursales_erp_precio_alquiler * this.sucursales_erp_cant)            
          }              
      

          subtotal = subtotal + Number(this.honorarios_de_implementacion)

          if (this.descuento_porcentaje > 0) {
            subtotal = subtotal - (this.descuento_porcentaje * subtotal / 100)
          }


          if (this.descuento_importe > 0) {
            subtotal = subtotal - this.descuento_importe
          }

          this.total_general = subtotal

          return subtotal
          }
        
        
      },
    watch: {
      servidor_erp: function (val, oldVal) {        
        this.calcular_honorarios()
      }, 
      servidor_erp_cant: function (val, oldVal) {        
        this.calcular_honorarios()
      }, 
      terminales_erp_cant: function (val, oldVal) {        
        this.calcular_honorarios()
      }, 
      sucursales_erp_cant: function (val, oldVal) {        
        this.calcular_honorarios()
      }, 
      descuento_porcentaje: function (val, oldVal) {        
        if(this.descuento_porcentaje > 100) {
          this.descuento_porcentaje = 100;
        }
        if(this.descuento_porcentaje < 0) {
          this.descuento_porcentaje = 0;
        }        
      },
      descuento_importe: function (val, oldVal) {        
        if(this.descuento_importe > this.total_general) {
          this.descuento_importe = 0;
        }
        if(this.descuento_importe < 0) {
          this.descuento_importe = 0;
        }        
      }
    },
    methods: {
      calcular_subtotal: function () {
        //this.honorarios_de_implementacion = this.servidor_val
      },
      buscar_paquete_por_id: function (id) {
        posicion = -1
        for (var i = 0, len = this.paquetes_erp.length; i < len; i++) {
          if (this.paquetes_erp[i].IdArticulo == id) {
            posicion = i
          }
        }
        return posicion
      },
      limpiar_paquetes: function () {
        console.log(this.modo_adquisicion)
        for (var i = 0, len = this.paquetes_erp.length; i < len; i++) {
          //console.log(this.paquetes_erp[i].IdArticulo+' - '+this.paquetes_erp[i].Nombre+' - '+this.paquetes_erp[i].chequeado+' - '+this.paquetes_erp[i].IdArticuloRel)
          if (this.modo_adquisicion != this.paquetes_erp[i].ModoAdquisicion) {  
              if (this.paquetes_erp[i].chequeado == true) {
                posicion = this.buscar_paquete_por_id(this.paquetes_erp[i].IdArticuloRel)  
                this.paquetes_erp[posicion].chequeado = true
                //console.log('paq-rel: pos('+posicion+') '+this.paquetes_erp[posicion].Nombre+' - '+this.paquetes_erp[posicion].IdArticulo+' - chek: '+this.paquetes_erp[posicion].chequeado)
              }    
          }
        }     
        this.calcular_honorarios()   
      },      
      calcular_honorarios: function () {
        if (this.modo_adquisicion == 'v') {
          this.honorarios_de_implementacion = (this.servidor_erp * this.servidor_erp_cant) + (this.sucursales_erp_precio_venta * this.sucursales_erp_cant) + (this.terminales_erp_precio * this.terminales_erp_cant)
          this.honorarios_de_implementacion = parseInt(this.honorarios_de_implementacion)
        }
        else {
          this.honorarios_de_implementacion = (this.servidor_erp * this.servidor_erp_cant) + (this.sucursales_erp_precio_alquiler * this.sucursales_erp_cant) + (this.terminales_erp_precio * this.terminales_erp_cant)
          this.honorarios_de_implementacion = parseInt(this.honorarios_de_implementacion)
        } 
      },
      validar_errores: function(){
        // VALIDO SI HAY ERRORES
        this.$validator.validateAll().then(() => {
            if (this.errors.any()) {
              // SI HAY ERRORES
              this.guardar = false
              this.mostrar_mensaje_error = true
              this.mensaje_error = 'Hay campos que corrergir'
            }
            else {
              // SI NO HAY ERRORES
              this.mostrar_mensaje_error = false
              this.guardar = true
              this.GenerarCotizacion()
            }
        }).catch(() => {
            this.title = this.errors;
        });
      },
      GenerarCotizacion: function(){
          this.btn_descargar_pdf = false
          
          if (this.modo_adquisicion == 'v') {
            sucursales_erp_precio = this.sucursales_erp_precio_venta
            sucursales_erp_id = this.sucursales_erp_id_venta
          }
          else {
            sucursales_erp_precio = this.sucursales_erp_precio_alquiler
            sucursales_erp_id = this.sucursales_erp_id_alquiler
          }              

          // GUARDO LA COTIZACION
          this.$http.post('guardar_cotizacion.php', {
            modo_adquisicion: this.modo_adquisicion,
            razon_social: this.razon_social,
            email: this.email,
            celular: this.celular,
            localidad: this.localidad,
            domicilio: this.domicilio,
            mostrar_paq_erp: this.mostrar_paq_erp,
            paquetes_erp: this.paquetes_erp,
            servidor_erp : this.servidor_erp,
            servidor_erp_cant : this.servidor_erp_cant,
            terminales_erp_cant: this.terminales_erp_cant,
            terminales_erp_precio: this.terminales_erp_precio,
            sucursales_erp_cant : this.sucursales_erp_cant,
            sucursales_erp_id : sucursales_erp_id,
            sucursales_erp_precio : sucursales_erp_precio,
            honorarios_de_implementacion: this.honorarios_de_implementacion,
            descuento_porcentaje: this.descuento_porcentaje,
            descuento_importe: this.descuento_importe,

         }).then(function(response){ 
            this.cotizacion_id = response.body
            //this.mensaje = response.body
            //this.mostrar_mensaje = true

            // GENERO EL PDF
            if (this.cotizacion_id != 'error') {
                 this.$http.post('generar_pdf.php', {
                    cotizacion_id: this.cotizacion_id

                 }).then(function(response){ 
                  //this.mensaje = response.body
                  //this.mostrar_mensaje = true                  
                    nombre_del_pdf = response.body;
                    if (nombre_del_pdf != 'error') {
                      //this.url_pdf = 'http://www.clasex.com.ar/nuevo-cotizador/archivos/'+nombre_del_pdf
                      this.url_pdf = 'http://localhost:1010/cotizador/new/archivos/'+nombre_del_pdf
                      this.mensaje_whastapp = 'https://api.whatsapp.com/send?phone=549'+this.celular+'&text=Estimado '+this.razon_social+'. ClaseX le envia la cotizacion solicitada. Para poder verla haga click en el siguiente enlace: '+this.url_pdf                      
                      this.btn_descargar_pdf = true
                    }
                 }, function(){
                    alert('Error2!')
                 });
            }

         }, function(){
            alert('Error1!')
         });
      },      
      EnviarCotizacionViaMail: function(){
          this.$http.post('enviar_email.php', {
            cotizacion_id: this.cotizacion_id

         }).then(function(response){ 
            this.mensaje_envio_mail = response.body
            this.mensaje = this.mensaje_envio_mail
            this.mostrar_mensaje = true

         }, function(){
            alert('Error!')
         });
      } 
    },

    filters: {
      formatoMoneda: function (value) {
        let val = (value/1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
      }
    }


  })



</script>
<!-- FIN vue.js -->


