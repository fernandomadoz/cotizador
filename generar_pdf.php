<?php

session_start();

session_cache_limiter();
date_default_timezone_set('America/Argentina/Buenos_Aires');
header("Content-Type: text/html; charset=iso-8859-1");

require_once ('conexion.php');


$request = json_decode(file_get_contents('php://input')); 

$IdCotizacion = $request->IdCotizacion;

//$IdCotizacion = 5;


if ($_SESSION['IdUsuario'] <> '') {

  //CREACION DEL PDF
  include('classPDF/class2.ezpdf.php');

  $pdf = new Cezpdf('a4');
  $pdf->setEncryption('','',array('copy','print'));
  $pdf->selectFont('classPDF/fonts/Helvetica.afm');



  $FECHA = date("d/m/Y");

  $query = "SELECT *, convert(varchar, Fecha, 103) Fecha_formato FROM Cotizaciones where IdCotizacion = $IdCotizacion";
  //echo "<br>query: ".$query;
  $rs_Cotizacion = sqlsrv_query($conn, $query);
  $row_Cotizacion = sqlsrv_fetch_array($rs_Cotizacion);
  $Numero = substr("0000000".$row_Cotizacion['Numero'], -8);
  $Fecha = $row_Cotizacion['Fecha_formato'];
  $nombre_del_pdf = $row_Cotizacion['nombre_del_pdf'];
  $RazonSocial = $row_Cotizacion['RazonSocial'];
  $Domicilio = $row_Cotizacion['Domicilio'];
  $Localidad = $row_Cotizacion['Localidad'];
  $Telefono = $row_Cotizacion['Telefono'];
  $modo_adquisicion = $row_Cotizacion['modo_adquisicion'];

  $a = utf8_decode("á");
  $e = utf8_decode("é");
  $i = utf8_decode("í");
  $o = utf8_decode("ó");
  $u = utf8_decode("ú");
  $O = utf8_decode("Ó");
  $I = utf8_decode("Í");

  $num = utf8_decode("º");

  $pdf->ezText("\n",2);
  $pdf->ezImage('img/logo-2013-membrete.jpg',0,230,'none','left');

  $pdf->ezText("        Rivadavia 644, Venado Tuerto, Santa Fe",11);
  $pdf->ezText("\n",1);
  $pdf->ezText("       www.clase-x.com.ar, (03462) 420749",11);
  $pdf->ezText("\n",1);
  $pdf->addText(295,800,20,"X",0);

  $pdf->addText(277,780,06,"DOCUMENTO NO",0);
  $pdf->addText(282,775,06,"VALIDO COMO",0);
  $pdf->addText(288,770,06,"FACTURA",0);
  $pdf->addText(360,800,12,"COTIZACION N".$num." ".$Numero,0);
  $pdf->addText(380,720,12,"FECHA  ".$Fecha."",0);

  $pdf->line(303,760,303,705);
  $pdf->setLineStyle(1);
  $pdf->line(30,705,565,705);
  $pdf->setLineStyle(1);
  $total_general = 0;

          
  $pdf->ezText("\n",6);
  $pdf->ezText("\n",3);
  $texto = "<b>$RazonSocial</b>";
  $pos = $pdf->ezText($texto,13,array('justification'=>'left'));
  $pdf->ezText("\n",3);
  $texto = "$Domicilio - $Localidad";
  $pos = $pdf->ezText($texto,13,array('justification'=>'left'));
  $pdf->ezText("\n",3);
  $texto = "Tel: $Telefono";
  $pos = $pdf->ezText($texto,13,array('justification'=>'left'));


  //------------------------------------------ERP-----------------------------------------------------
  $query = "SELECT * FROM CotizacionDetalle cd LEFT JOIN Paquetes2017 p ON cd.IdArticulo = p.IdArticulo where cd.IdCotizacion = $IdCotizacion AND (cd.IdArticulo IS NOT NULL AND cd.IdArticulo not in (1509, 1865))";
    
  $rs_erp = sqlsrv_query( $conn, $query , array(), array( "Scrollable" => SQLSRV_CURSOR_KEYSET )); 
  
  $pdf->setColor(0.9,0.9,0.9);
  $pdf->filledRectangle(30, 561, 534, 50);
  $pdf->setColor(0,0,0);
  $pdf->ezText("\n",9);
  $pdf->ezText('',5,array('justification'=>'left'));
  $texto = "<b>$modo_adquisicion</b>";
  $pos = $pdf->ezText($texto,13,array('justification'=>'right'));
  $pdf->ezImage('img/logo_gextion_erp_factura.jpg',5,100,'none','left');
  $pos = 564;
  $pdf->setLineStyle(1);  
  $pdf->setStrokeColor(1,0,0);
  $pdf->line(30,$pos-2,565,$pos-2);
  $pdf->setStrokeColor(0.7,0.7,0.7);


  // DATOS CLIENTE

  $j = 0;
  if (sqlsrv_num_rows($rs_erp) > 0) {  
    //PAQUETES ERP
    $pdf->setColor(0.5,0.5,0.5);
    while($row_erp = sqlsrv_fetch_array($rs_erp)) { 

      if ($j<1) {
        $j = 1;
      } 
      else {
        $pdf->ezText("\n",2);
        $pdf->setLineStyle(0.2);  
        $pdf->line(30,$pos-11,565,$pos-11);        
        $pdf->ezText("\n",5);        
      }

      $pdf->setColor(0,0,0);
      $codigo = strtoupper($row_erp['Codigo']);
      $nombre = strtoupper($row_erp['Nombre']);
      $array_nombre_modulos = explode(";", $row_erp['Modulos']);
      $array_carcateristicas = explode(";", $row_erp['Caracteristicas']);
      $precio = $row_erp['Precio'];
      $txt_precio = "$ ".number_format($precio, 0, ',', '.');

      $texto = "<b>$nombre</b> (<i>$codigo</i>)";
      $pos = $pdf->ezText($texto,11,array('justification'=>'left'));
      $pdf->ezSetY($pos+14);
      $pdf->ezText($txt_precio,12,array('justification'=>'right')); 
      $pdf->ezText("\n",2);

      for($k=0; $k<count($array_nombre_modulos); $k++) {
        $pdf->setColor(0.5,0.5,0.5);
        $nombre_modulo = $array_nombre_modulos[$k];
        $carcateristicas = $array_carcateristicas[$k];
        $texto = "<b>$nombre_modulo:</b> <i>$carcateristicas</i>";
        $pos = $pdf->ezText($texto,9,array('justification'=>'left')); 
      }
    
    //$pdf = printline($pdf);

  

    }


    // SERVIDOR
    $query = "SELECT Cantidad, PrecioFinal FROM CotizacionDetalle cd where IdCotizacion = $IdCotizacion AND Descripcion = 'servidor'";
    //echo "<br>query: ".$query;
    $rs_servidor = sqlsrv_query($conn, $query);  
    $row_servidor = sqlsrv_fetch_array($rs_servidor);
    $Cantidad = intval($row_servidor['Cantidad']);
    $PrecioFinal = $row_servidor['PrecioFinal'];

    $subtotalerp = $Cantidad*$PrecioFinal;
    
    $pdf->setColor(0,0,0);

    //$precio = $row_erp['Precio'];
    $txt_precio = "$ ".number_format($PrecioFinal*$Cantidad, 0, ',', '.');

    $pdf->ezText("\n",2);
    $pdf->setLineStyle(0.2);  
    $pdf->line(30,$pos-11,565,$pos-11);
    $pdf->ezText("\n",5);

    $texto = "<b>Servidor</b> (<i>Cant: $Cantidad</i>)";
    $pos = $pdf->ezText($texto,11,array('justification'=>'left'));
    $pdf->ezSetY($pos+15);
    $pdf->ezText($txt_precio,13,array('justification'=>'right')); 
    //$pdf = printline($pdf);




    // TERMINALES
    $query = "SELECT Cantidad, PrecioFinal FROM CotizacionDetalle cd where IdCotizacion = $IdCotizacion AND Descripcion = 'Terminales Adicionales'";
    //echo "<br>query: ".$query;
    $rs_terminales = sqlsrv_query($conn, $query);  
    $row_terminales = sqlsrv_fetch_array($rs_terminales);
    $Cantidad = intval($row_terminales['Cantidad']);
    $PrecioFinal = $row_terminales['PrecioFinal'];

    if ($Cantidad > 0) {
      $subtotalerp = $subtotalerp + ($Cantidad*$PrecioFinal);
    }
    
    $pdf->setColor(0,0,0);

    //$precio = $row_erp['Precio'];
    $txt_precio = "$ ".number_format($PrecioFinal*$Cantidad, 0, ',', '.');

    $pdf->ezText("\n",2);
    $pdf->setLineStyle(0.2);  
    $pdf->line(30,$pos-11,565,$pos-11);
    $pdf->ezText("\n",5);

    $texto = "<b>Terminales Adicionales</b> (<i>Cant: $Cantidad</i>)";
    $pos = $pdf->ezText($texto,11,array('justification'=>'left'));
    $pdf->ezSetY($pos+15);
    $pdf->ezText($txt_precio,13,array('justification'=>'right')); 
    //$pdf = printline($pdf);

    // SUCURSALES
    $query = "SELECT Cantidad, PrecioFinal FROM CotizacionDetalle cd where IdCotizacion = $IdCotizacion AND Descripcion = 'Sucursales'";
    //echo "<br>query: ".$query;
    $rs_sucursales = sqlsrv_query( $conn, $query , array(), array( "Scrollable" => SQLSRV_CURSOR_KEYSET )); 
    $row_sucursales = sqlsrv_fetch_array($rs_sucursales);
    $PrecioFinal = $row_sucursales['PrecioFinal'];
    $Cantidad = intval($row_sucursales['Cantidad']);

    if ($Cantidad > 0) {
      $subtotalerp = $subtotalerp + ($Cantidad*$PrecioFinal);
    }
      
    
    $pdf->setColor(0,0,0);

    //$precio = $row_erp['Precio'];
    $txt_precio = "$ ".number_format($PrecioFinal*$Cantidad, 0, ',', '.');

    $pdf->ezText("\n",2);
    $pdf->setLineStyle(0.2);  
    $pdf->line(30,$pos-11,565,$pos-11);
    $pdf->ezText("\n",5);

    $texto = "<b>Sucursales</b> (<i>Cant: $Cantidad</i>)";
    $pos = $pdf->ezText($texto,11,array('justification'=>'left'));
    $pdf->ezSetY($pos+15);
    $pdf->ezText($txt_precio,13,array('justification'=>'right')); 
    
    //$pdf = printline($pdf);




    //HONORARIOS PROFESIONALES
    $query = "SELECT PrecioFinal FROM CotizacionDetalle cd where IdCotizacion = $IdCotizacion AND Descripcion = 'Honorarios de implementacion'";
    //echo "<br>query: ".$query;
    $rs_honorarios = sqlsrv_query( $conn, $query , array(), array( "Scrollable" => SQLSRV_CURSOR_KEYSET )); 

    $row_honorarios = sqlsrv_fetch_array($rs_honorarios);
    $PrecioFinal = $row_honorarios['PrecioFinal'];
    $subtotalerp = $subtotalerp + $PrecioFinal;
    
    $pdf->setColor(0,0,0);

    //$precio = $row_erp['Precio'];
    $txt_precio = "$ ".number_format($PrecioFinal, 0, ',', '.');

    $pdf->ezText("\n",2);
    $pdf->setLineStyle(0.2);  
    $pdf->line(30,$pos-11,565,$pos-11);
    $pdf->ezText("\n",5);

    $texto = "<b>Honorarios Profesionales</b>";
    $pos = $pdf->ezText($texto,11,array('justification'=>'left'));
    $pdf->ezSetY($pos+15);
    $pdf->ezText($txt_precio,13,array('justification'=>'right')); 
    //$pdf = printline($pdf);




    // DESCUENTOS
    $query = "SELECT PrecioFinal FROM CotizacionDetalle cd where IdCotizacion = $IdCotizacion AND Descripcion = 'Importe de Descuento'";
    //echo "<br>query: ".$query;
    $rs_descuento = sqlsrv_query( $conn, $query , array(), array( "Scrollable" => SQLSRV_CURSOR_KEYSET )); 

    if (sqlsrv_num_rows($rs_descuento) > 0) { 
      
      $row_descuento = sqlsrv_fetch_array($rs_descuento);
      $PrecioFinal = $row_descuento['PrecioFinal'];
      $subtotalerp = $subtotalerp + $PrecioFinal;
      
      $pdf->setColor(0,0,0);

      //$precio = $row_erp['Precio'];
      $txt_precio = "$ ".number_format($PrecioFinal, 0, ',', '.');

      $pdf->ezText("\n",2);
      $pdf->setLineStyle(0.2);  
      $pdf->line(30,$pos-11,565,$pos-11);
      $pdf->ezText("\n",5);

      $texto = "<b>Descuento</b>";
      $pos = $pdf->ezText($texto,11,array('justification'=>'left'));
      $pdf->ezSetY($pos+15);
      $pdf->ezText($txt_precio,13,array('justification'=>'right')); 
      //$pdf = printline($pdf);

    }

    


  }


  

  //------------------------------------------VALOR TOTAL-----------------------------------------------------
  $pdf->setColor(1,0,0);
  $pdf->ezText("\n",5);
  $pdf->ezText("________________________________________________________________________________________________",10);
  $pdf->ezText("\n",3);
  $pdf->setColor(0,0,0);
  $txt_precio = "<b>Total:</b>   $ ".number_format($subtotalerp, 0, ',', '.');

  $pdf->ezText($txt_precio,15,array('justification'=>'right')); 

  $pdf->setColor(1,0,0);
  $pdf->ezText("________________________________________________________________________________________________",10);
  $pdf->ezText("\n",5);
  $pdf->setColor(0,0,0);
  $txt_precio = "<b>Total:</b>   $ ".number_format($subtotalerp, 0, ',', '.');





  $pdf->ezText("\n",10);        
  $pdf->ezText("* Todos los precios INCLUYEN IVA 21%",10);  
  $pdf->ezText("\n",3);  
  $pdf->ezText("* Cotizaci".$o."n v".$a."lida por 30 d".$i."as\n",10);
  
  //$pdfcode = $pdf->ezStream();

  ob_end_clean();
  $output = $pdf->ezOutput(); //Salida de archivo

  $rutaArchivoPdf = 'archivos/'.$nombre_del_pdf;
  $url_pdf = 'http://www.clasex.com.ar/nuevo-cotizador/'.$rutaArchivoPdf;

  file_put_contents($rutaArchivoPdf, $output); //guardar en el server
  //$pdf->ezStream();//abrir el pdf de forma automatica

  // FIN CREACION DEL PDF

  //mail('caffeinated@example.com', 'Mi título', $mensaje);

  echo $nombre_del_pdf ;
}
else {
  echo json_encode('error');  
}
?>