
<!-- saved from url=(0050)http://www.siemenroorda.nl/class.backgroundpdf.txt -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">&lt;?php

  class backgroundPDF extends Cezpdf
  {
    var $bg_type;
    var $bg_color = array();
    var $bg_img;
    var $bg_img_width;
    var $bg_img_height;
    var $bg_img_xpos;
    var $bg_img_ypos;
    var $bg_img_type;
    var $bg_img_types_jpg = array('jpg', 'jpeg', 'pjpg', 'pjpeg');
    var $bg_img_types_png = array('png');

    /****************************************
     * backgroundPDF
     * -------------
     * Input:
     *   $paper       : similar to Cezpdf-constructor
     *   $orientation : similar to Cezpdf-constructor
     *   $type        : { 'none' | 'color' | 'colour' | 'image' }
     *   $options     : if type == 'color' or 'colour':
     *                    $options['r'] = red-component   of backgroundcolour ( 0 &lt;= r &lt;= 1)
     *                    $options['g'] = green-component of backgroundcolour ( 0 &lt;= g &lt;= 1)
     *                    $options['b'] = blue-component  of backgroundcolour ( 0 &lt;= b &lt;= 1)
     *                  if type == 'image':
     *                    $options['img']    = location of image file; URI's are allowed if allow_url_open is enabled in php.ini
     *                    $options['width']  = width of background image; default is width of page
     *                    $options['height'] = height of background image; default is height of page
     *                    $options['xpos']   = horizontal position of background image; default is 0
     *                    $options['ypos']   = vertical position of background image; default is 0
     ****************************************/
    function backgroundPDF($paper = 'a4', $orientation = 'portrait', $type = 'none', $options = array())
    {
      $this-&gt;Cezpdf($paper, $orientation);
      switch ($type)
      {
        case 'color'  :
        case 'colour' :
                        $this-&gt;bg_type = 'colour';
                        $this-&gt;saveState();
                        if (is_float($options['r']) &amp;&amp; is_float($options['g']) &amp;&amp; is_float($options['b']))
                        {
                          $this-&gt;bg_color = array('r' =&gt; $options['r'], 'g' =&gt; $options['g'], 'b' =&gt; $options['b']);
                          $this-&gt;setColor($this-&gt;bg_color['r'], $this-&gt;bg_color['g'], $this-&gt;bg_color['b'], 1);
                        }
                        else
                          $this-&gt;bg_color = array(1, 1, 1);
                        $this-&gt;filledRectangle(0, 0, $this-&gt;ez['pageWidth'], $this-&gt;ez['pageHeight']);
                        $this-&gt;restoreState();
                        break;
        case 'image'  :
                        $this-&gt;bg_type = 'image';
                        if (isset($options['img']))
                        {
                          $bg_img_types = array_merge($this-&gt;bg_img_types_jpg, $this-&gt;bg_img_types_png);
                          $bg_img_ext = strtolower(substr($options['img'], strpos($options['img'], '.') + 1));

                          if (is_array($this-&gt;bg_img_types_jpg) &amp;&amp; in_array($bg_img_ext, $this-&gt;bg_img_types_jpg))
                            $this-&gt;bg_img_type = 'jpg';
                          else if (is_array($this-&gt;bg_img_types_png) &amp;&amp; in_array($bg_img_ext, $this-&gt;bg_img_types_png))
                            $this-&gt;bg_img_type = 'png';
                          else
                            $this-&gt;bg_img_type = NULL;
                        }
                        else
                          $this-&gt;bg_img_type = NULL;

                        // Don't continue if image does not exist, or if image has a wrong type
                        if (!isset($options['img']) || (!isset($this-&gt;bg_img_type)) || !file_exists($options['img']))
                        {
                        if (!file_exists($options['img']))
                          echo $options['img'];
                          $this-&gt;bg_type = 'none';
                          break;
                        }
                        $this-&gt;bg_img = $options['img'];
                        if (is_numeric($options['width']))  $this-&gt;bg_img_width  = $options['width'];  else $this-&gt;bg_img_width  = $this-&gt;ez['pageWidth'];
                        if (is_numeric($options['height'])) $this-&gt;bg_img_height = $options['height']; else $this-&gt;bg_img_height = $this-&gt;ez['pageHeight'];
                        if (is_numeric($options['xpos']))   $this-&gt;bg_img_xpos   = $options['xpos'];   else $this-&gt;bg_img_xpos   = 0;
                        if (is_numeric($options['ypos']))   $this-&gt;bg_img_ypos   = $options['ypos'];   else $this-&gt;bg_img_ypos   = 0;
                        switch ($this-&gt;bg_img_type)
                        {
                          case 'jpg' : $this-&gt;addJpegFromFile($this-&gt;bg_img, $this-&gt;bg_img_xpos, $this-&gt;bg_img_ypos, $this-&gt;bg_img_width, $this-&gt;bg_img_height); break;
                          case 'png' : $this-&gt;addPngFromFile($this-&gt;bg_img, $this-&gt;bg_img_xpos, $this-&gt;bg_img_ypos, $this-&gt;bg_img_width, $this-&gt;bg_img_height);  break;
                        }
                        break;
        case 'none'   :
        default      :
                        $this-&gt;bg_type = 'none';
                        break;
      }
    }

    /****************************************
     * ezNewPage
     * ---------
     * Just an overloading function
     ****************************************/
    function ezNewPage($insert = 0, $id = 0, $pos = 'after')
    {
      parent::ezNewPage($insert, $id, $pos);
      switch ($this-&gt;bg_type)
      {
        case 'colour' :
                        $this-&gt;saveState();
                        $this-&gt;setColor($this-&gt;bg_color['r'], $this-&gt;bg_color['g'], $this-&gt;bg_color['b'], 1);
                        $this-&gt;filledRectangle(0, 0, $this-&gt;ez['pageWidth'], $this-&gt;ez['pageHeight']);
                        $this-&gt;restoreState();
                        break;
        case 'image'  :
                        switch ($this-&gt;bg_img_type)
                        {
                          case 'jpg' : $this-&gt;addJpegFromFile($this-&gt;bg_img, $this-&gt;bg_img_xpos, $this-&gt;bg_img_ypos, $this-&gt;bg_img_width, $this-&gt;bg_img_height); break;
                          case 'png' : $this-&gt;addPngFromFile($this-&gt;bg_img, $this-&gt;bg_img_xpos, $this-&gt;bg_img_ypos, $this-&gt;bg_img_width, $this-&gt;bg_img_height);  break;
                        }
                        break;
        case 'none'   :
        default      :
                        break;
      }
    }
  }

?&gt;
</pre></body></html>