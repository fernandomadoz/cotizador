<?php 

session_start();
session_cache_limiter();

date_default_timezone_set('America/Argentina/Buenos_Aires');
header("Content-Type: text/html; charset=iso-8859-1");

require_once ('conexion.php');

if (isset($_GET['e'])) {
  $Mensaje = 'Usuario o Clave incorrectos, ingr&eacute;selos nuevamente';
}

if (isset($_GET['x'])) {
  session_unset();
  session_destroy();
  session_start();
  $Mensaje = 'Ha salido del Cotizador';
}

?>
<!doctype html>
<html lang="es">
  <head>
    <meta http-equiv="content-language" content="es">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/logo_shortcut.ico" />

    <title>Login - Presupuestador Clase-X</title>

    <!-- Bootstrap core CSS -->
    <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee|Montserrat" rel="stylesheet">
  </head>

  <body>

    <div id="encabezado">
      <div id="centro">
        <div class="container contenedor_encabezado">
          <div class="row">
            <img src="img/logo.png">
            <img class="hidden-xs  pull-right" src="img/titulo-presupuestador.png">
            <img class="visible-xs  pull-right" style="width: 150px;" src="img/titulo-presupuestador.png">
          </div>
        </div>
        <div class="container contenedor_principal">


      <?php 
      if(isset($Mensaje)) {
          ?>
          <div class="alert alert-danger cartel-mensaje" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $Mensaje; ?>
          </div>
          <?php
      }
      ?>

        
            <form class="form-signin" action="cotizador.php" method="POST">
              <h2 class="form-signin-heading">Identif&iacute;quese</h2>
              <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Usuario" required autofocus>
              <input type="password" id="clave" name="clave" class="form-control" placeholder="Contrase&ntilde;a" required>
              <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
            </form>


        </div> <!-- /container -->
      </div>  <!-- /centro -->  
    </div>  <!-- /encabezado -->    
  </body>
</html>

